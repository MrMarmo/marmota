import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import org.testng.annotations.*;

import java.io.IOException;

public class clientTest {
    MarmoClient client;
    public void init(String url){
        client=new MarmoClient(url);
        System.out.println("Test class initialized");
    }
    @Test(groups={"serverCode"})
    public void getServerCode(){
        client=new MarmoClient("http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452");
        System.out.println(client.getServerResponseCode());
    }
    @Test(groups={"content"})
    public void getURLContent(){
        client=new MarmoClient("http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452");
        System.out.print(client.getURLContent());
    }
    @Test(groups={"parseContent"})
    public void getName(){
        client=new MarmoClient("http://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452");
        System.out.println(client.getAdressComponentName(client.getURLContent(),"administrative_area_level_1",true));
    }
    private void  parseJsonObject(JsonReader reader){
        try {
            reader.beginObject();
            while(reader.hasNext()){
                JsonToken token=reader.peek();
                if(token.equals(JsonToken.BEGIN_ARRAY))
                    parseJsonArray(reader);
                else if(token.equals(JsonToken.END_OBJECT)){
                    reader.endObject();
                    return;
                }
                else parseJsonToken(reader);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void parseJsonArray(JsonReader reader){
        try {
            reader.beginArray();
            while(true){
                JsonToken token=reader.peek();
                if(token.equals(JsonToken.END_ARRAY)){
                    reader.endArray();
                    return;
                }
                else if(token.equals(JsonToken.BEGIN_OBJECT))parseJsonObject(reader);
                else if(token.equals(JsonToken.END_OBJECT))reader.endObject();
                else parseJsonToken(reader);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void parseJsonToken(JsonReader reader){

    }
}
