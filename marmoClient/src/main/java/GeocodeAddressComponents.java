import com.google.gson.annotations.SerializedName;

public class GeocodeAddressComponents{
    @SerializedName("long_name")
    public String longName;
    @SerializedName("short_name")
    public String shortName;
    public  String[] types;
}
