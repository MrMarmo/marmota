
import com.google.gson.Gson;

import java.net.*;
import java.io.*;
public class MarmoClient {
    public URL url;
    public HttpURLConnection connection;
    public MarmoClient(String web){
        try {
            url=new URL(web);
            connection=(HttpURLConnection)url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public int getServerResponseCode(){
        if(connection!=null){
            try {
                return connection.getResponseCode();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
    public String getURLContent(){
        if(connection!=null){
            try {
                BufferedReader reader=new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder content=new StringBuilder();
                String input;
                while((input=reader.readLine())!=null){
                    content.append(input);
                }
                return content.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
    public String getAdressComponentName(String json,String addressType,boolean getLongName){
        Geocode geocode=new Gson().fromJson(json,Geocode.class);
        if(geocode!=null && geocode.geocodeResults!=null){
            for(int x=0;x< geocode.geocodeResults.length;x++){
                if(geocode.geocodeResults[x]!=null){
                    GeocodeResult result= geocode.geocodeResults[x];
                    for(int y=0;y<result.addressComponents.length;y++){
                        String[] types=result.addressComponents[y].types;
                        String name= result.addressComponents[y].shortName;
                        if(getLongName)name=result.addressComponents[y].longName;
                        for(int z=0;z<types.length;z++)
                            if(types[z].equals(addressType))
                                return name;
                    }
                }
            }
        }
        return "";
    }
    public String getFormattedAddress(String json){
        Geocode geocode=new Gson().fromJson(json,Geocode.class);
        if(geocode!=null && geocode.geocodeResults!=null){
            for(int x=0;x< geocode.geocodeResults.length;x++){
                if(geocode.geocodeResults[x]!=null){
                    GeocodeResult result= geocode.geocodeResults[x];
                    return result.formattedAddress;
                }
            }
        }
        return "";
    }
}
